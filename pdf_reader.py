'''PDF Reader using tesseract-ocr example
This program will fetch a few available PDFs from the internet and convert them to text using tesseract-ocr.
You must install tesseract-ocr in order for this to work, as well as the python packages pytesseract, pdf2image,
and requests using pip.

https://github.com/tesseract-ocr/tesseract/wiki
'''

import pytesseract
import pdf2image
import requests

pdf_array = [
    "http://solutions.weblite.ca/pdfocrx/scansmpl.pdf",
    "https://idrh.ku.edu/sites/idrh.ku.edu/files/files/tutorials/pdf/Non-text-searchable.pdf",
    "https://www.fujitsu.com/global/Images/sv600_m_normal.pdf",
    "https://www.ets.org/Media/Tests/GRE/pdf/gre_research_validity_data.pdf"]

for pdf_url in pdf_array:
    pdf_doc = requests.get(pdf_url).content
    pages = pdf2image.convert_from_bytes(pdf_doc)

    for page in pages:
        print(pytesseract.image_to_string(page))